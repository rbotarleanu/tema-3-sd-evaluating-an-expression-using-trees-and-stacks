#include "arbori.h"

TArb InitA ()
{
    return NULL;
}

TArb ConstrNod(TInfo x, TArb s, TArb d)
{ TArb aux = (TArb)malloc(sizeof(TNod));  /* incearca alocare spatiu */
  if (!aux) return NULL; /* spatiu insuficient */
  aux->info = (TInfo)calloc(1,sizeof(TIcel));
  aux->info->nume = (char*)malloc(strlen(x->nume)+1);
  if(!aux->info->nume){free(aux); return NULL;}
  aux->info->val = -1;
  strcpy(aux->info->nume,x->nume); aux->st = s; aux->dr = d; /* actualizeaza campurile nodului */
  return aux;                              /* intoarce adresa nodului */
}

void distruge(TArb r) 	/* functie auxiliara - distruge toate nodurile */
{ if (!r) return;
  distruge (r->st);     /* distruge subarborele stang */
  distruge (r->dr);     /* distruge subarborele drept */
  free(r->info->nume);
  free(r->info);
  free(r);             /* distruge nodul radacina */
  r=NULL;
}

void DistrArb(TArb *a) /* distruge toate nodurile arborelui de la adresa a */
{ if (!(*a)) return;       /* arbore deja vid */
  distruge (*a);           /* distruge toate nodurile */
  *a = NULL;               /* arborele este vid */
}



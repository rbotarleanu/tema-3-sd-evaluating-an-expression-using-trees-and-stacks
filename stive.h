#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#ifndef STIVE_H_INCLUDED
#define STIVE_H_INCLUDED

#define VF(a) ((AST)(a))->vf
#define DIME(a) ((AST)(a))->dime

typedef struct nod_s
{
    void *info;
    struct nod_s *urm;
} TCelulaG, *TLG, **ALG;

typedef struct
{ size_t dime;
  TLG vf;
} TStiva, *AST;

void *InitS(size_t d,...);		//initializare stiva
int Push(void *s, void *ae);	//inserare in varf
int Pop(void *s, void *ae);		//extragere din varf
int Top(void *s, void *ae);		//copiere din varf
int isEmpty(void *s);

void ResetS(void *s);			//resetare stiva
void DistrS(void **as);			//distrugere stiva

int InvS(void *a);				//inversare stiva

#endif // STIVE_H_INCLUDED

#include "stive.h"

void *InitS(size_t d,...)
{
	AST s;
	s = (TStiva *)calloc(1,sizeof(TStiva));
	if(!s) return NULL;
	s->vf = NULL;
	s->dime = d;
	return s;
}

void ResetS(void *s)		//reseteaza lista de valori a stivei
{
	ALG aL = &VF(s);
	TLG aux;
	while(*aL)
	{
		aux=*aL;
		*aL=(*aL)->urm;
		aux->info = NULL;
        free(aux->info);
		free(aux);
	}
}

void DistrS(void **as)		//functie distrugere stiva
{
	ResetS(*as);
	free(*as);
}

int Push(void *s, void *ae)		//push pentru un element in stiva
{
   	if(!VF(s) || VF(s)->info)	//varful e celula cu informatie utila
	{
		TLG aux = calloc(1,sizeof(TCelulaG));
		if(!aux) return 0;
		aux->info=malloc(((AST)s)->dime);
    	if(!aux->info) {free(aux); return 0;}

		memcpy(aux->info,ae, ((AST)s)->dime);
		aux->urm = VF(s);
		VF(s)=aux;
	}
	return 1;
}

int isEmpty(void *s)            //test stiva vida
{
	return (VF(s) == NULL) || (VF(s)->info == NULL) ;
}

int Pop(void *s, void *ae)	//extragerea elementului din varf
{
	if(isEmpty(s)) return 0;	//test stiva vida

	TLG aux=VF(s);

	memcpy(ae,aux->info, ((AST)s)->dime);
	VF(s) = aux->urm;
	free(aux->info);
    free(aux);
	return 1;
}

int Top(void *s, void *ae)   //copiaza elementul din varful stivei
							 //la adresa ae
{
    if(isEmpty(s)) return 0;	//test stiva vida
    TLG aux=((AST)s)->vf;
    memcpy(ae,aux->info, ((AST)s)->dime);
	return 1;
}

#include "header_tema.h"
#include "arbori.h"
#include "stive.h"

int is_operand(char c)
{
    return c == '*' || c == '/' || c == '+' || c == '-';
}

char *transform_infix(char *E, int *nrop)  //transforma expresie din infix in notatie poloneza(postfix)
{
    *nrop = 0;
    void *operand_stack = InitS(sizeof(char));	//stiva pentru operatori
    char *PE = (char*)calloc(1,strlen(E)+1);	//expresia transformata
    int j=0;
    int i =0;
    while(E[i]!='\0' && E[i]!='\n')
    {
        if(E[i]=='(') { i++; continue; }	//nu afecteaza
        if(E[i]==')')		//extrag un operator din stiva
        {
            char ae = NULL;
            Pop(operand_stack,&ae);
            PE[j++] = ae;	//il inserez in expresia transformata
            i++;
            continue;
        }
        if(is_operand(E[i])) {Push(operand_stack,&E[i]);(*nrop)++;}
					//este operator, il inserez in stiva
        else PE[j++] = E[i];	//operand, il adaug in expresia trsnformata
        i++;
    }
    PE[j] = '\0';
    DistrS(&operand_stack);	//distrug stiva de operatori
    return PE;
}

TArb construct_tree(char *E, TArb *Arez)   //construieste arborul binar pentru expresia E
{
    void *tree_stack = InitS(sizeof(TArb));	//stiva de arbori folosita pentru
											//constructia arborelui final
    if(!tree_stack) return NULL;
    void *tree_stack2 = InitS(sizeof(TArb));
    if(!tree_stack2){DistrS(&tree_stack); return NULL;}

    int i = 0;
    while(E[i] != '\0' && E[i] != '\n')
    {
        TInfo nod = (TInfo)calloc(1,sizeof(TIcel));
        nod->nume = (char*)malloc(2);
        nod->nume[0] = E[i]; nod->nume[1]='\0';

        if(!is_operand(E[i]))
        {//operand, va fi adaugat in stiva
            TArb lit = InitA();
            TArb lit2 = InitA();
            lit = ConstrNod(nod,NULL,NULL);
            if(!lit) {DistrS(tree_stack); return NULL;}
            lit2 = ConstrNod(nod,NULL,NULL);
            if(!lit2){DistrArb(&lit); DistrS(tree_stack); DistrS(tree_stack2); return NULL;}
            Push(tree_stack,&lit);
            Push(tree_stack2,&lit2);
        }
        else
        {//operator, ultimele doua elemente din stiva sunt extrase, iar radacina lor
		//va fi acest operand
            TArb dr = NULL, st = NULL;
            TArb dr2 = NULL, st2 = NULL;
            Pop(tree_stack2,&dr2);
            Pop(tree_stack,&dr);
            Pop(tree_stack2,&st2);
            Pop(tree_stack,&st);

            TArb oper = ConstrNod(nod,st,dr);
            if(!oper) {DistrS(tree_stack); return NULL;}
            TArb oper2 = ConstrNod(nod,st2,dr2);
            if(!oper2){DistrArb(&oper); DistrS(tree_stack); DistrS(tree_stack2);}
            Push(tree_stack,&oper);
            Push(tree_stack2,&oper2);
        }
        i++;
        free(nod->nume);
        free(nod);
    }
    TArb rez;
    Pop(tree_stack,&rez);	//arborele de expresie construit
    Pop(tree_stack2,Arez); //arborele rezerva pentru cerinta 3
    DistrS(&tree_stack);
    DistrS(&tree_stack2);
    return rez;
}

void afisare_arboreRSD(TArb a, FILE *fout)
{//afisare in preordine
    if(!a) return;
    if(!a->st && !a->dr) {fprintf(fout,"%s", a->info->nume); return;}
	fprintf(fout, "%s", a->info->nume);
	fprintf(fout, "(");
	afisare_arboreRSD(a->st, fout);
	fprintf(fout, ",");
	afisare_arboreRSD(a->dr, fout);
	fprintf(fout, ")");
}

int get_value(char c, char *numeVar, int *valVar, int n)
{//cauta si intoarce valoarea unui termen
  int i =0;
  for(; i < n;i++ ) if(numeVar[i] == c) break;
  return valVar[i];
}

void afisare_arboreSDR(TArb a, FILE *fout, int *valVar, char *numeVar, int n)
{//afiseaza in postordine si inlocuieste valorile variabilelor
    if(!a) return;
    if(!a->st && !a->dr)
    {
        if(!is_operand(a->info->nume[0]))
            {//operand, ii este afisata valoarea
                a->info->val = get_value(a->info->nume[0],numeVar,valVar,n);
                fprintf(fout,"%d", a->info->val);
            }
        else fprintf(fout,"%c", a->info->nume[0]);//operator, este afisat
    return;}

    fprintf(fout,"(");
    afisare_arboreSDR(a->st, fout,valVar,numeVar,n);
    fprintf(fout,",");
	afisare_arboreSDR(a->dr, fout,valVar,numeVar,n);
	fprintf(fout,")");
    if(!is_operand(a->info->nume[0]))
    {
        a->info->val = get_value(a->info->nume[0],numeVar,valVar,n);
        fprintf(fout,"%d", a->info->val);
    }
    else fprintf(fout,"%c", a->info->nume[0]);
}

int calculate(int va, int vb, char op)
{//in functie de tipul operatorului, calculeaza valoarea obtinuta
    switch(op)
    {
        case '*': return va*vb; break;
        case '+': return va+vb; break;
        case '-': return va-vb; break;
        default: return va/vb; break;
    }
}

void evaluare_arbore(TArb a, FILE *fout, char *numeVar, int *valVar, int n)
{//evalueaza arborele si afiseaza in postordine
    if(!a) return;
    if(a->st && a->dr)
    {
        if(!is_operand(a->st->info->nume[0]) && !is_operand(a->dr->info->nume[0]))
        {//fii sunt operanzi, deci radacina este operator. Se evalueaza.
            int va, vb;
            if(a->st->info->nume[0] == '#') va = a->st->info->val;
                else va = get_value(a->st->info->nume[0],numeVar,valVar,n);
            if(a->dr->info->nume[0] == '#') vb = a->dr->info->val;
                else vb = get_value(a->dr->info->nume[0],numeVar,valVar,n);

            a->info->val = calculate(va,vb,a->info->nume[0]);
				//valoarea data de expresie
            TArb aux = a->st;
            TArb aux2 = a->dr;
            a->info->nume[0] = '#';
				//pentru a retie faptul ca nodul respectiv a fost evaluat
            a->st = a->st->st;	//distrug arcele la nodurile fii si distrug fiii
            a->dr = a->dr->dr;
            aux->st = aux->dr = NULL;
            aux2->dr = aux2->st = NULL;
            distruge(aux);
            distruge(aux2);
        }
    }
    if(!a->st && !a->dr)
    {//este operator
        if(a->info->nume[0] =='#' ) fprintf(fout,"%d",a->info->val);
			//valoarea calculata
        else if(!is_operand(a->info->nume[0]))
            {//este operand, nu a fost folosit in vreun calcul momentan si se afiseaza valoarea
                a->info->val = get_value(a->info->nume[0],numeVar,valVar,n);
                fprintf(fout,"%d", a->info->val);
            }
        else fprintf(fout,"%c", a->info->nume[0]);
			//operand nefolosit in calcul; se afiseaza
    return;}

    fprintf(fout,"(");
    evaluare_arbore(a->st, fout,numeVar,valVar,n);
    fprintf(fout,",");
	evaluare_arbore(a->dr, fout,numeVar,valVar,n);
	fprintf(fout,")");
    if(!is_operand(a->info->nume[0]))
    {
        a->info->val = get_value(a->info->nume[0],numeVar,valVar,n);
        fprintf(fout,"%d", a->info->val);
    }
    else if(a->info->nume[0] =='#' ) fprintf(fout,"%d",a->info->val);
            else fprintf(fout,"%c", a->info->nume[0]);
}

int match_expression(Expresie e, char *st, char *dr, char op)
{//daca doua expresii sunt egale returneaza 1, 0 altfel
    if(op != e.op) return 0;
    if(op == '/' || op == '-') return !strcmp(e.st,st) && !strcmp(e.dr,dr);
    return (!(strcmp(e.st,dr))&&!strcmp(e.dr,st)) || (!strcmp(e.st,st) && !strcmp(e.dr,dr));
}

int get_expresion(Expresii bank, char *st, char *dr, char op, int nre)
{//cauta expresia; daca o gaseste returneaza pozitia, in caz contrar -1
    int i = 0;
    for(; i < nre; i++)
        if(match_expression(bank[i],st,dr,op)) return i;
    return -1;
}

void DistrBank(Expresii *bank, int nre)
{//distruge bank-ul de expresii
    int i =0;
    for(; i< nre; i++)
    {
        free((*bank)[i].st);
        free((*bank)[i].dr);
        free((*bank)[i].nume);
    }
    free(*bank);
}

void add_expression(Expresii bank, TArb st, TArb dr, char op, int *nr_exp, char *numeVar, int *valVar, int n)
{//adauga expresie in bank; inclusiv calculeaza valoarea sa
    char nume_expresie[255], numar[100];
    sprintf(numar,"%d",(*nr_exp)+1);
    nume_expresie[0] = 'E'; nume_expresie[1]='\0';
    strcpy(nume_expresie+1,numar);
    bank[*nr_exp].nume = (char*)malloc(strlen(nume_expresie)+1);        //se afla numele expresiei si se copiaza
    if(!bank[*nr_exp].nume) return;
    strcpy(bank[*nr_exp].nume,nume_expresie);

    bank[*nr_exp].st = (char*)malloc(strlen(st->info->nume)+1);         //se copiaza numele fiului stang
    if(!bank[*nr_exp].st) return;
    strcpy(bank[*nr_exp].st, st->info->nume);

    bank[*nr_exp].dr = (char*)malloc(strlen(dr->info->nume)+1);         //se copiaza numele fiului drept
    if(!bank[*nr_exp].dr) {free(bank[*nr_exp].st);  return;}
    strcpy(bank[*nr_exp].dr, dr->info->nume);

    bank[*nr_exp].op = op;

    int val_st, val_dr;     //se determina valoarea expresiei
    if(st->info->nume[0] == 'E') val_st = st->info->val;        //fiul stang e expresie
        else val_st = get_value(st->info->nume[0],numeVar,valVar,n);
    if(dr->info->nume[0] == 'E') val_dr = dr->info->val;        //fiul drept e expresie
        else val_dr = get_value(dr->info->nume[0],numeVar,valVar,n);
    bank[*nr_exp].val = calculate(val_st,val_dr,op);    //se actualizeaza valoarea
    (*nr_exp)++;        //creste numarul de expresii
}

void print_SDR(TArb a, FILE *fout)      //afiseaza in postordine, fara sa tina cont de valorile expresiei
{   if(!a) return;
    if(!a->st && !a->dr)
    {   fprintf(fout,"%s", a->info->nume);
        return;
    }
    fprintf(fout, "(");
    print_SDR(a->st, fout);
    fprintf(fout, ",");
    print_SDR(a->dr, fout);
    fprintf(fout, ")");
    fprintf(fout, "%s", a->info->nume);
}

void optimizare(TArb a, FILE *fout, Expresii bank, int *nr_exp, char *numeVar, int *valVar, int n )
{//optimizeaza
   if(!a) return;
   if(!a->st && !a->dr) return;
   if(!is_operand(a->st->info->nume[0]) && !is_operand(a->dr->info->nume[0]))
   {//operatie posibil de evaluat
        int find = get_expresion(bank,a->st->info->nume,a->dr->info->nume,a->info->nume[0],*nr_exp);
        if(find == -1)
        {//expresie noua, se adauga la bank
            add_expression(bank,a->st,a->dr,a->info->nume[0],nr_exp,numeVar,valVar,n);
            DistrArb(&a->st);   //fiii sunt distrusi
            DistrArb(&a->dr);
            int bc = (*nr_exp)-1;   //expresia curenta
            a->info->val = bank[bc].val;
            free(a->info->nume);
            a->info->nume = (char*)malloc(strlen(bank[bc].nume)+1);
            if(!a->info->nume) return;
            strcpy(a->info->nume,bank[bc].nume);

            fprintf(fout,"%s=(%s,%s)%c=%d\n",bank[bc].nume, bank[bc].st, bank[bc].dr, bank[bc].op, bank[bc].val);

        }
        else
        {
            free(a->info->nume);    //expresia deja exista, se inlocuieste numele radacinii si valoarea sa
            a->info->nume = (char*)malloc(strlen(bank[find].nume)+1);
            if(!a->info->nume) return;
            strcpy(a->info->nume, bank[find].nume);
            DistrArb(&a->st);
            DistrArb(&a->dr);
            a->info->val = bank[find].val;
        }
   }
    optimizare(a->st,fout,bank,nr_exp,numeVar,valVar,n);    //optimizare in recusrivitate
    optimizare(a->dr,fout,bank,nr_exp,numeVar,valVar,n);
}




#include "arbori.h"
#include "stive.h"
#include "header_tema.h"

int main( int argc, char *argv[] )
{
    FILE *fin = fopen(argv[1],"rt");
    if(!fin) {printf("File not found:%s\n", argv[1]); return 2;}
    FILE *fout = fopen(argv[2],"wt");
    if(!fout){printf("File not found:%s\n",argv[2]); fclose(fin); return 2;}

    char nume,egal;
    fscanf(fin, "%c %c", &nume,&egal ); //delete plz
    char *E, buffer[1000];

    fgets(buffer,1000,fin);
    E = (char*)malloc(strlen(buffer)+1);	//expresia data
    if(!E) return 2;
    strcpy(E, buffer);

    int nr_operatori;
    char *PE = transform_infix(E,&nr_operatori);	//expresia data este transformata
													//in forma poloneza

    TArb expresie2 = NULL;
    TArb expresie = construct_tree(PE,&expresie2);		//se construieste arborele de expresie

    afisare_arboreRSD(expresie,fout);	//afisare in preordine
    fprintf(fout, "\n");

    int n;
    fscanf(fin, "%d\n", &n);
    char *numeVar = (char *)calloc(1,n+1);		//numele operanzilor
    int *valoareVar = (int *)calloc(sizeof(int),n+1);	//valorile lor
    int i = 0;
    for(; i < n; i++)
        fscanf(fin,"%c %d\n", &numeVar[i], &valoareVar[i]);
				//citesc numele si valorile variabilelor
    afisare_arboreSDR(expresie,fout,valoareVar,numeVar,n);
				//afisez in postordine si inlocuiesc numele cu valorile operanzilor
    fprintf(fout,"\n");

    while(expresie->info->val == -1)    //cat timp e operatie radacina arborelui
    {//evaluez arborele, pana cand rezultatul final al expresiei se afla in radacina
        evaluare_arbore(expresie,fout,numeVar,valoareVar,n);
        fprintf(fout,"\n");
    }

    Expresii bank = (TArb*)malloc(sizeof(TArb)*nr_operatori);
    int nrex = 0;

    do
    {
        print_SDR(expresie2,fout);
        fprintf(fout,"\n");
        optimizare(expresie2,fout,bank,&nrex,numeVar,valoareVar,n);
    }while(expresie2->st && expresie2->dr);
    fprintf(fout,"%d",expresie2->info->val);


	DistrBank(&bank,nrex);
	free(numeVar);
    free(valoareVar);
    free(E);
    free(PE);
    DistrArb(&expresie);
    DistrArb(&expresie2);
    fclose(fin);
    fclose(fout);
    return 0;
}

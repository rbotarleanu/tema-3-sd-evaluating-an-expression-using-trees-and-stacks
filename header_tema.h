#include "arbori.h"
#include "stive.h"

#ifndef HEADER_TEMA_H_INCLUDED
#define HEADER_TEMA_H_INCLUDED

int is_operand(char c); //intoarce 1 daca c este *, -, +, / si 0 in rest
char *transform_infix(char *E, int *nrop);  //transforma expresie din infix in notatie poloneza(postfix)
TArb construct_tree(char *E, TArb *Arez);   //construieste arborul binar pentru expresia E si o copie a sa
void afisare_arboreSDR(TArb a, FILE *fout, int *valVar, char *numeVar, int n);
    //afiseaza in postordine si inlocuieste valorile variabilelor
typedef struct
{
    char *nume;
    char *st, *dr;
    char op;
    int val;
}Expresie, *Expresii;
void evaluare_arbore(TArb a, FILE *fout, char *numeVar, int *valVar, int n);
		//evalueaza arborele, pentru fiecare nod radacina operator cu doi fii operanzi
		//calculeaza valoarea expresiei intermediare si distruge nodurile fii
void afisare_arboreSDR(TArb a, FILE *fout, int *valVar, char *numeVar, int n);
		//afiseaza in postordine si inlocuieste numele variabilelor cu valorile lor
void afisare_arboreRSD(TArb a, FILE *fout);
		//afiseaza in preordine
void print_SDR(TArb a, FILE *fout); //afisare in postordine
void DistrBank(Expresii *bank, int nre);    //distruge expresiile retinute
int get_expresion(Expresii bank, char *st, char *dr, char op, int nre);
    //cauta expresia; daca o gaseste returneaza pozitia, in caz contrar -1
void add_expression(Expresii bank, TArb st, TArb dr, char op, int *nr_exp, char *numeVar, int *valVar, int n);
    //adauga expresie in bank; inclusiv calculeaza valoarea sa
void print_SDR(TArb a, FILE *fout);      //afiseaza in postordine, fara sa tina cont de valorile expresiei
void optimizare(TArb a, FILE *fout, Expresii bank, int *nr_exp, char *numeVar, int *valVar, int n );
    //optimizeaza expresia conform cerintei 3


#endif // HEADER_TEMA_H_INCLUDED

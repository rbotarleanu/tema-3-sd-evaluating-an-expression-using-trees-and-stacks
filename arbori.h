#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include <string.h>

#ifndef ARBORI_H_INCLUDED
#define ARBORI_H_INCLUDED

typedef struct
{
    char *nume;
    int val;
}TIcel,*TInfo;

typedef struct nod
{
    TInfo info;
    struct nod *st, *dr;
} TNod, *TArb, **AArb;

// Operatii elementare pentru arbori
TArb InitA();  //initializare arbore
TArb ConstrNod(TInfo x, TArb s, TArb d);
    // x -> info, s-> fiu stanga, d-> fiu dreapta... Null daca nu e spatiu
void DistrArb(AArb);
void distruge(TArb r); //distruge noduri pornind de la radacina
#endif // ARBORI_H_INCLUDED
